### Hello there! 
# Matheus Martins

<div>

- 💻 Infraestrutura como código e a cultura DevOps.
- 🌱 Apaixonado por automação, tecnologia e mundo GNU/Linux.

</div>

<div>
  <a href = "mailto:3mhenrique@gmail.com"><img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/matheushsmartins" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a>   
</div>
- This is the way! 
